package com.th7.hwmdmcontrol;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent huawei =new Intent();
        huawei.setAction("com.drupe.swd.launcher.huoshan.mdm.service.ExecuteCmdService");
        huawei.putExtra("cmd","command_release_control");
        huawei.putExtra("active",1);//optional
        huawei.setPackage("com.android.launcher3");
        this.startService(huawei);
    }
}