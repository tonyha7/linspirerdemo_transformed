//
// Decompiled by Jadx - 819ms
//
package android.app.mia;

import android.content.Context;
import java.util.ArrayList;
import java.util.List;

public class MiaMdmPolicyManager {
    private List<String> list = new ArrayList();
    private String s = null;

    public MiaMdmPolicyManager(Context paramContext) {
    }

    public boolean allowBluetoothDataTransfer(boolean paramBoolean) {
        return false;
    }

    public List<String> appWhiteListRead() {
        return this.list;
    }

    public boolean appWhiteListWrite(List<String> list2) {
        return false;
    }

    public void controlApp(String paramString, boolean paramBoolean) {
    }

    public void deleteLockPattern(int paramInt) {
    }

    public String getBtStatus() {
        return this.s;
    }

    public String getWifiStatus() {
        return this.s;
    }

    public void masterClearInbulitSD() {
    }

    public void setBackKey(boolean paramBoolean) {
    }

    public void setCamera(boolean paramBoolean) {
    }

    public boolean setGps(boolean paramBoolean) {
        return false;
    }

    public void setHomeKey(boolean paramBoolean) {
    }

    public void setNavigaBar(boolean paramBoolean) {
    }

    public void setOTA(boolean paramBoolean) {
    }

    public void setPowerLongPressKey(boolean paramBoolean) {
    }

    public void setPowerSingleClickKey(boolean paramBoolean) {
    }

    public void setRecentKey(boolean paramBoolean) {
    }

    public void setTFcard(boolean paramBoolean) {
    }

    public void setUsbOnlyCharging(boolean paramBoolean) {
    }

    public void setVoiceSize(boolean paramBoolean) {
    }

    public void setVolumedownKey(boolean paramBoolean) {
    }

    public void setVolumeupKey(boolean paramBoolean) {
    }

    public void setWifiProxy(boolean paramBoolean) {
    }

    public void silentInstall(String paramString) {
    }

    public void silentUnInstall(String paramString) {
    }

    public boolean updateSystemTime(String paramString) {
        return false;
    }

    public boolean urlSetEnable(boolean paramBoolean) {
        return false;
    }

    public List<String> urlWhiteListRead() {
        return this.list;
    }

    public boolean urlWhiteListWrite(List<String> list2) {
        return false;
    }
}
