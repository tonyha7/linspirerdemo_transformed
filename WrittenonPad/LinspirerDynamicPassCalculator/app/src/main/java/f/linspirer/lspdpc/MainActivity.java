package f.linspirer.lspdpc;

import android.app.*;
import android.os.*;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.EditText;
import android.content.Context;
import android.widget.Toast;
import android.view.View;
import android.app.Activity;
import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;
import java.util.Date;
import java.text.SimpleDateFormat;

public class MainActivity extends Activity 
{
	String key500 = "1191ADF1-8489-D8DA-5E9B-755A8B674394-485SDEWQ-QWYHK586";
	String key501 = "40E06F51-30D0-D6AD-7F7D-008AD0ADC570";
	public void OnClick(final View view) {
        try {
            switch (view.getId()) {
                case R.id.button: {
						
						String swdid = ((EditText)this.findViewById(R.id.et)).getText().toString();
				    	String pass500 = calc(swdid,key500);
						String pass501 = calc(swdid,key501);
				    	TextView tvs = (TextView) findViewById(R.id.tv);
				    	tvs.setText("5.0.***:"+pass500+"\n5.01.***:"+pass501);
						break;
					}
            }
        }
        catch (Exception ex) {}
    }
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
	
	/*
	 领创apk里扣的代码 微改
	 renamed from: com.innofidei.guardsecure.util.c
	 loaded from: classes.dex
	 */

	public static String calc(String str,String key) {
        String str2 = new SimpleDateFormat("yyyyMMdd").format(new Date()) + str + key;
		String b = m3759b(str2);
		if (b.length() > 8) {
			return m3758c(b.substring(b.length() - 8, b.length()));
		}
        return "";
    }

    /* renamed from: b */
    public static String m3759b(String str) {
        try {
            byte[] bytes = str.getBytes();
            MessageDigest instance = MessageDigest.getInstance("md5");
            instance.reset();
            instance.update(bytes);
            byte[] digest = instance.digest();
            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                String hexString = Integer.toHexString(b & 255);
                if (hexString.length() == 1) {
                    hexString = "0" + hexString;
                }
                sb.append(hexString);
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    /* renamed from: c */
    public static String m3758c(String str) {
        String valueOf = String.valueOf(Long.parseLong(str, 16));
        if (valueOf.length() <= 8) {
            return valueOf;
        }
        return valueOf.substring(valueOf.length() - 8, valueOf.length());
    }
}
