package com.th7.webviewtest;

import static android.content.ContentValues.TAG;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private String ENTER_EMAIL ="######";
    private String ENTER_PASSWORD ="######";
    final String fem = String.format("javascript:document.getElementById('email').value='"+ENTER_EMAIL+"'");
    final String fpw = String.format("javascript:document.getElementById('password').value='"+ENTER_PASSWORD+"'");
    private WebView mWebView;
    private MainActivity self;
    private String url ="http://cloud.linspirer.com:880/login";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        self = MainActivity.this;
        mWebView = (WebView) findViewById(R.id.wv);
        mWebView.getSettings().setAllowContentAccess(true);
        mWebView.getSettings().setAppCacheEnabled(false);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.getSettings().setDatabaseEnabled(true);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setGeolocationEnabled(false);
        mWebView.getSettings().setSaveFormData(true);
        mWebView.getSettings().setSavePassword(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mWebView.getSettings().setLoadsImagesAutomatically(true);
        CookieManager mCookieManager = CookieManager.getInstance();
        mCookieManager.setAcceptCookie(true);
        mCookieManager.setAcceptThirdPartyCookies(mWebView, true);
        mWebView.setWebChromeClient(new WebChromeClient()
        {
            @Override
            public void onProgressChanged(WebView view, int newProgress)
            {
                super.onProgressChanged(view, newProgress);
                view.requestFocus();
            }
        });

        mWebView.setWebViewClient(new MyWebViewClient(self));
        mWebView.loadUrl(url);
    }

    private String random() {Random random = new Random();
        //范围内的随机�?        int min = 1;
        int max = 8000000;
        int num = random.nextInt(max - min + 1) + min;
        Log.e(TAG, "random: " + num);
        String s=Integer.toString(num);
        Toast.makeText(MainActivity.this, s, Toast.LENGTH_LONG).show();
        return s;
    }

    public void onClick(View opt) {
        switch(opt.getId()){
            case R.id.loginb:
                mWebView.loadUrl(url);
                break;
            case R.id.pwbt:
                mWebView.evaluateJavascript(fem,null);
                mWebView.evaluateJavascript(fpw,null);
                break;
            case R.id.comment0:
                mWebView.loadUrl("http://cloud.linspirer.com:880/application/"+random()+"/changestatus/0");
                break;
            case R.id.comment1:
                mWebView.loadUrl("http://cloud.linspirer.com:880/application/"+random()+"/changestatus/1");
                break;
            default:
                break;
        }
    }


}