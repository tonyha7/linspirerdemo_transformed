package com.th7.webviewtest;

import android.app.Activity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class MyWebViewClient extends WebViewClient {
    String url;
    WebView view;
    Activity activity;
    TextView textView;
    public MyWebViewClient(Activity activity)
    {
        //  this.textView =textView;
        this.activity =activity;
    }

    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }

    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        //view.loadUrl("javascript:document.getElementById('email').value='admin@wp.com'");
        //view.loadUrl("javascript:document.getElementById('password').value='abc123'");
    }

}