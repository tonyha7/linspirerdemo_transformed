package de.szalkowski.activitylauncher;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ComponentInfo;
import android.os.Bundle;
import android.widget.Toast;

import com.th7.wisdomclass.linspirerdemo.R;

import org.thirdparty.LauncherIconCreator;

public class RootLauncherIconCreator {
    public static void createLauncherIcon(Context context, MyActivityInfo activity) {
        Signer signer = new Signer(context);
        Bundle extras = new Bundle();
        ComponentName comp = activity.getComponentName();

        String signature;
        try {
            signature = signer.signComponentName(comp);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, context.getText(R.string.error).toString() + ": " + e.toString(), Toast.LENGTH_LONG).show();
            return;
        }

        extras.putString("pkg", comp.getPackageName());
        extras.putString("cls", comp.getClassName());
        extras.putString("sign", signature);

        activity.is_private = true;
        activity.component_name = new ComponentName("de.szalkowski.activitylauncher", "de.szalkowski.activitylauncher.RootLauncherActivity");

        LauncherIconCreator.createLauncherIcon(context, activity, extras);
    }
}
