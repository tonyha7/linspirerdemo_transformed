package com.th7.newUI;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.innofidei.guardsecure.dataclean.PullNewsActivity;
import com.th7.classic.MainActivity;
import com.th7.wisdomclass.linspirerdemo.R;

public class UIPicker extends AppCompatActivity {
    //private CountDownTimer timer;
    //private TextView timertv=(TextView) findViewById(R.id.countdown);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_ui);
        //setTimer();
    }
    public void onClick(View opt) {
        switch(opt.getId()) {
            case R.id.miko:
                classic();
                break;
            case R.id.entrance:
                hw();
                break;
            default:
                break;
        }
    }
/*
    @Override
    protected void onDestroy() {
        super.onDestroy();
        destoryTimer();
    }

    private void setTimer() {
        timer = new CountDownTimer(10 * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int time = (int) (millisUntilFinished / 1000);
                timertv.setText("You will be redirected to the classical UI in "+time +" seconds.");
            }
            @Override
            public void onFinish() {
                timertv.setText("You will be redirected to the classical UI.");
                destoryTimer();
                classic();
            }
        };
        timer.start();
    }

    private void destoryTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }


 */
    private void classic() {
        Intent intent = new Intent(this,MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        }
    private void hw() {
        Intent intent = new Intent(this,PullNewsActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
    private void modern() {
        //todo
    }
}
