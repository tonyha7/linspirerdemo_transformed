package com.th7.utils.mdm;

public class MDMType {
    private boolean CSDK(){
        try{
            Class.forName("android.app.csdk.CSDKManager");
        }
        catch (ClassNotFoundException e){
            return false;
        }
        return true;
    }
    private boolean Mia(){
        try{
            Class.forName("android.app.mia.MiaMdmPolicyManager");
        }
        catch (ClassNotFoundException e){
            return false;
        }
        return true;
    }
    public int MDM(){
        if(CSDK() && !Mia()){
            return 2;
        }
        if(!CSDK() && Mia()){
            return 3;
        }
        if(CSDK() && Mia()){
            return 3;
        }
        return 1;
    }
}
