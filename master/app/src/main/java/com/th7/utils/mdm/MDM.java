package com.th7.utils.mdm;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.th7.utils.RootCommand;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class MDM {
    int MDM = new MDMType().MDM();
    int Mia = 3;
    int CSDK = 2;
    Context context;

    public MDM(Context context) {
        this.context = context;
    }

    public void load() {
        if (MDM == CSDK) {
            CSDKUtil csdkUtil = new CSDKUtil(context);
            csdkUtil.load();
        } else if (MDM == Mia) {
            Toasty.warning(context, "Mia适配ing", Toast.LENGTH_SHORT, true).show();
        } else {
            Toasty.warning(context, "无管控", Toast.LENGTH_SHORT, true).show();
        }
        controlkeys();
    }

    public void addAppWhite(String PackageName) {
        if (MDM == CSDK) {
            CSDKUtil csdkUtil = new CSDKUtil(context);
            csdkUtil.addappwhite(PackageName);
        } else if (MDM == Mia) {

        } else {

        }
    }

    public void uninstallApp(String PackageName) {
        if (MDM == CSDK) {
            CSDKUtil csdkUtil = new CSDKUtil(context);
            csdkUtil.UninstallApp(PackageName);
        } else if (MDM == Mia) {

        } else {
            Uri uri = Uri.fromParts("package", PackageName, null);
            Intent intent = new Intent(Intent.ACTION_DELETE, uri);
            context.startActivity(intent);
        }
    }

    public void controlapp(String PackageName, boolean on) {
        if (MDM == CSDK) {
            CSDKUtil csdkUtil = new CSDKUtil(context);
            csdkUtil.controlapp(PackageName, on);
        } else if (MDM == Mia) {

        } else {

        }
    }

    public void controlkeys() {
        if (MDM == CSDK) {
            CSDKUtil csdkUtil = new CSDKUtil(context);
            csdkUtil.controlkeys();
        } else if (MDM == Mia) {

        } else {

        }
    }

    public void releasekeys() {
        if (MDM == CSDK) {
            CSDKUtil csdkUtil = new CSDKUtil(context);
            csdkUtil.releasekeys();
        } else if (MDM == Mia) {

        } else {

        }
    }

    public void back2linspirer() {
        if (MDM == CSDK) {
            CSDKUtil csdkUtil = new CSDKUtil(context);
            csdkUtil.back2linspirer();
        } else if (MDM == Mia) {

        } else {

        }
    }

    public void Block_Linspirer_Network() {
        if (MDM == CSDK) {
            CSDKUtil csdkUtil = new CSDKUtil(context);
            csdkUtil.Block_Linspirer_Network();
        } else if (MDM == Mia) {

        } else {

        }
    }

    public void KillAppBackGround(String PackageName) {
        if (MDM == CSDK) {
            CSDKUtil csdkUtil = new CSDKUtil(context);
            csdkUtil.KillApp(PackageName);
        } else if (MDM == Mia) {

        } else {
            RootCommand.runCommand("am force-stop "+PackageName);
        }
    }

    public String SN() {
        if (MDM == CSDK) {
            CSDKUtil csdkUtil = new CSDKUtil(context);
            return csdkUtil.SN();
        }
        return "null";
    }

    public static ArrayList getAllApp(@NonNull Context context) {
        ArrayList list1 = new ArrayList<>();//架空白名单
        PackageManager pm = context.getPackageManager();
        List<PackageInfo> packages = pm.getInstalledPackages(0);
        for (PackageInfo packageInfo : packages) {
            if ((packageInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {
                if (packageInfo.packageName.equals("com.android.launcher3") || packageInfo.packageName.equals("com.ndwill.swd.appstore")) {
                    continue;
                }
                list1.add(packageInfo.packageName+";miahash");
            }

        }
        return list1;
    }

    public void FlashAppWhiteList(@NonNull Context context) {
        ArrayList Installed = getAllApp(context);
        if (MDM == CSDK) {
            CSDKUtil csdkUtil = new CSDKUtil(context);
            csdkUtil.Addappwhitelist(Installed);
        } else if (MDM == Mia) {

        } else {

        }
    }
}