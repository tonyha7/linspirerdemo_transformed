package com.th7.utils;

import java.util.ArrayList;
import java.util.List;

public class ListMerge {

    /**
     * 合并List ListAddALL
     *
     * @param list1 待合并的List1
     * @param list2 待合并的List2
     * @param <T>   泛型
     * @return
     */
    public static <T> List<T> Merge(List<T> list1, List<T> list2) {
        List<T> mergeList = new ArrayList<>();
        mergeList.addAll(list1);
        mergeList.addAll(list2);
        return mergeList;
    }

}
