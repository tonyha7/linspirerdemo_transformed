package com.th7.utils.mdm;

import android.app.csdk.CSDKManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.widget.Toast;

import com.lzf.easyfloat.EasyFloat;
import com.th7.utils.ListMerge;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class CSDKUtil {
    Context context;
    CSDKManager csdk;

    public CSDKUtil(Context context) {
        this.context = context;
    }

    public void load() {
        csdk = new CSDKManager(context);

        try{
            csdk.removeDeviceOwner("vizpower.imeeting");
            csdk.setDeviceOwner(context.getPackageName()+"/com.th7.wisdomclass.linspirerdemo.DeviceReceiver");
        }
        catch (Exception E){
        }

        csdk.setSafeModeDisabled(false);
        csdk.disableHiddenGame(false);
        csdk.disableWifiDirect(false);
        csdk.enableDevMode(true);
        csdk.disableWifi(false);
        try {
            csdk.setPackageEnabled("com.android.launcher3", false);
        } catch (Exception e) {
        }
        csdk.SetEnable(false);
        csdk.enableMassStorage(true);
        csdk.setUsbCharging(false);
        csdk.enableUsbDebugging(true);
        csdk.setCustomOTG(true);
        csdk.allowTFcard(true);
        //新增
        csdk.disableInstallation(false);
        csdk.disableStatusBarNotification(false);
        csdk.disableStatusBarPanel(false);
        csdk.disableUnInstallation(false);
        csdk.enableSIM(true);
        csdk.enableData(true);
        csdk.fullScreenForever(false);
        csdk.disableBluetooth(false);
        csdk.allowBluetoothDataTransfer(true);
        csdk.disableBluetoothShare(false);
        csdk.enableMassStorage(true);
        csdk.disableAutostart(true);
        Toasty.success(context, "CSDK加载完毕", Toast.LENGTH_SHORT, true).show();
    }

    public void back2linspirer(){
        csdk = new CSDKManager(context);
        csdk.hideBackSoftKey(true);

        try {
            csdk.setPackageEnabled("com.android.launcher3", true);
        } catch (Exception e) {
        }

        csdk.disableWifi(false);
        csdk.SetEnable(true);
    }

    public void Block_Linspirer_Network(){
        csdk = new CSDKManager(context);
        int cnt=20;
        while((cnt--)>0){
            ArrayList aRead = (ArrayList) csdk.urlBlackListRead();
            aRead.add("linspirer.com");
            csdk.urlBlackListWrite(aRead);
            try {
                Thread.sleep(300); //300 毫秒，也就是0.3秒.
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    public void addappwhite(String PackageName){
        csdk = new CSDKManager(context);
        ArrayList list;
        list = (ArrayList) csdk.getInstallPackageWhiteList();
        list.add(PackageName+";miahash");
        csdk.addInstallPackageWhiteList(list);
    }

    public void releasekeys(){
        csdk = new CSDKManager(context);
        csdk.hideHomeSoftKey(false);
        csdk.hideBackSoftKey(false);
        csdk.hideMenuSoftKey(false);
    }

    public void controlkeys(){
        csdk = new CSDKManager(context);
        csdk.hideHomeSoftKey(true);
        csdk.hideBackSoftKey(false);
        csdk.hideMenuSoftKey(true);
    }

    public void controlapp(String PackageName,boolean on){
        csdk = new CSDKManager(context);
        try {
            csdk.setPackageEnabled(PackageName, on);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void KillApp(String PackageName){
        csdk = new CSDKManager(context);
        csdk.killApplicationProcess(PackageName);
    }

    public void UninstallApp(String PackageName){
        csdk = new CSDKManager(context);
        csdk.silentUnInstall(PackageName);
    }

    public String SN(){
        csdk = new CSDKManager(context);
        return csdk.getDeviceInfo(2);
    }

    public void Addappwhitelist(ArrayList AppList) {
        csdk = new CSDKManager(context);
        ArrayList list1;
        list1 = (ArrayList) csdk.getInstallPackageWhiteList();
        ArrayList list = (ArrayList) ListMerge.Merge(list1,AppList);
        csdk.removeInstallPackageWhiteList(list1);
        csdk.addInstallPackageWhiteList(list);
    }
}
