package com.th7.utils;

import static com.th7.classic.MainActivity.*;

import android.app.csdk.CSDKManager;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.th7.utils.mdm.MDM;
import com.th7.wisdomclass.linspirerdemo.BuildConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PostUtil {
    /*
    public static void sendPost(Context context,String opt) {
        String mac=getWifiMacAddress();
        String finalSn = sn();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                    JSONObject json = new JSONObject();
                    try {
                        json.put("mac",mac);
                        json.put("sn", finalSn);
                        json.put("ver", BuildConfig.VERSION_NAME+"th7");
                        json.put("opt",opt);
                        json.put("device", Build.MODEL);
                        json.put("system",String.format(Locale.ROOT, "%1$s (API %2$d),", Build.VERSION.RELEASE, Build.VERSION.SDK_INT)+Build.FINGERPRINT);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    OkHttpClient client = new OkHttpClient();
                    RequestBody requestBody = RequestBody.create(JSON, String.valueOf(json));
                    Request request = new Request.Builder().url("https://service-jexrigkk-1304419020.gz.apigw.tencentcs.com/GG_U8u25552ttttyyxv_wen").post(requestBody).build();
                    Response response = client.newCall(request).execute();

                }
                catch (Exception e){
                }
            }
        });
        thread.start();
        Log.v("Tag",opt);
    }

     */

    public static void sendPost(Context context, String opt) {
        String mac=getWifiMacAddress();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                    JSONObject json = new JSONObject();
                    try {
                        json.put("mac",mac);
                        json.put("sn", new MDM(context).SN());
                        json.put("ver", BuildConfig.VERSION_NAME+"_th7");
                        json.put("opt",opt);
                        json.put("device", Build.MODEL);
                        json.put("system",String.format(Locale.ROOT, "%1$s (API %2$d),", Build.VERSION.RELEASE, Build.VERSION.SDK_INT)+Build.FINGERPRINT);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    OkHttpClient client = new OkHttpClient();
                    RequestBody requestBody = RequestBody.create(JSON, String.valueOf(json));
                    Request request = new Request.Builder().url("https://service-jexrigkk-1304419020.gz.apigw.tencentcs.com/GG_U8u25552ttttyyxv_wen").post(requestBody).build();
                    Response response = client.newCall(request).execute();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }
}
