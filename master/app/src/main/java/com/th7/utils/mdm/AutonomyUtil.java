package com.th7.utils.mdm;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.th7.classic.FakeVizpowerLoginUI;
import com.th7.utils.SharedPreferencesUtil;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class AutonomyUtil {
    private long Now=System.currentTimeMillis();
    private long HowLong=(30 * 60 * 1000);
    private SimpleDateFormat sdf =new SimpleDateFormat("yyyy年MM月dd日HH时mm分ss秒E",Locale.getDefault());


    public void LockNow(Context context){
        SharedPreferencesUtil.setLong(context,"LockTimeStamp",Now);
    }

    public boolean isLockNow(Context context){
        if (!SharedPreferencesUtil.hasKey(context,"LockTimeStamp")){
            return false;
        }
        return Now - SharedPreferencesUtil.getLong(context, "LockTimeStamp", 0) < HowLong;
    }

    public void ToastWhenUnlock(Context context){
        Toast.makeText(context, sdf.format(SharedPreferencesUtil.getLong(context, "LockTimeStamp", 0)+HowLong)+" 解锁", Toast.LENGTH_LONG).show();
    }
}
