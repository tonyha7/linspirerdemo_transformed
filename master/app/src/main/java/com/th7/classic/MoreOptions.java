package com.th7.classic;

import android.app.csdk.*;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

import com.th7.wisdomclass.linspirerdemo.DeviceMethod;
import com.th7.wisdomclass.linspirerdemo.R;
import com.th7.utils.PostUtil;

import es.dmoral.toasty.Toasty;

public class MoreOptions extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {super.onCreate(savedInstanceState);setContentView(R.layout.activity_moreoptions);
    }

    public void 恢复出厂(){
        DeviceMethod.getInstance(this).WipeData();}
    public void 启用相机(){new CSDKManager(this).disableCamera(false);Toasty.success(this, "相机已启用", Toast.LENGTH_SHORT, true).show();}
    public void 禁用相机(){new CSDKManager(this).disableCamera(true);Toasty.error(this, "相机已禁用", Toast.LENGTH_SHORT, true).show();}
    public void 启用恢复出厂(){new CSDKManager(this).disableFactoryReset(false);Toasty.success(this, "恢复出厂设置已启用", Toast.LENGTH_SHORT, true).show();}
    public void 禁用恢复出厂(){new CSDKManager(this).disableFactoryReset(true);Toasty.error(this, "恢复出厂设置已禁用", Toast.LENGTH_SHORT, true).show();}
    public void 启用多用户(){new CSDKManager(this).disableMultiUser(false);Toasty.success(this, "多用户已启用", Toast.LENGTH_SHORT, true).show();}
    public void 禁用多用户(){new CSDKManager(this).disableMultiUser(true);Toasty.error(this, "多用户已禁用", Toast.LENGTH_SHORT, true).show();}


    public void onClick(View opt) {
        switch(opt.getId()){
            case R.id.b1:
                禁用相机();
                break;
            case R.id.b2:
                启用相机();
                break;
            case R.id.b3:
                Intent intent1 = new Intent();
                intent1.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings$DeviceAdminSettingsActivity"));
                startActivity(intent1);
                break;
            case R.id.b4:
                禁用多用户();
                break;
            case R.id.b5:
                启用多用户();
                break;
            case R.id.b6:
                Intent intent0=new Intent(Intent.ACTION_DELETE);intent0.setData(Uri.parse("package:com.android.launcher3"));intent0.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);startActivity(intent0);
                break;

            case R.id.wd:
                EditText cwd1 = (EditText) findViewById(R.id.wipedata);String cwd = cwd1.getText().toString();
                if(cwd.equals("Wipe")){
                    PostUtil.sendPost(getApplicationContext(),"设备恢复出厂");
                    DeviceMethod.getInstance(this).allowFactoryReset();
                    启用恢复出厂();
                    恢复出厂();
                    Toasty.error(this, "如果你看到这行字 请去主界面取消保护应用", Toast.LENGTH_SHORT, true).show();
                }
                else{Toasty.error(this, "请确认", Toast.LENGTH_SHORT, true).show();}
                break;
            case R.id.setCustomSDUPDATEenable:
                    new CSDKManager(this).setCustomSDUPDATE(true);
                break;
            case R.id.setCustomSDUPDATEdisable:
                    new CSDKManager(this).setCustomSDUPDATE(false);
                break;
            default:
                break;
        }
    }
}
