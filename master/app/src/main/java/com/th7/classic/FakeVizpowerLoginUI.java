package com.th7.classic;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tencent.bugly.Bugly;
import com.tencent.bugly.crashreport.CrashReport;
import com.th7.newUI.UIPicker;
import com.th7.utils.MD5Util;
import com.th7.utils.SignUtil;
import com.th7.utils.mdm.AutonomyUtil;
import com.th7.wisdomclass.linspirerdemo.BuildConfig;
import com.th7.wisdomclass.linspirerdemo.DeviceMethod;
import com.th7.wisdomclass.linspirerdemo.R;


public class FakeVizpowerLoginUI extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!BuildConfig.BUILD_TYPE.equals("debug")) {
            int signature = SignUtil.getSignature(getApplicationContext());
            if (!MD5Util.digest(String.valueOf(signature)).equals("dd8120779a005bdbbc932487ab2d4999")) {
                //DeviceMethod.getInstance(this).WipeData();
                // 可能被重编译了，需要退出
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        }

        DeviceMethod.getInstance(this).onActivate();
        CrashReport.initCrashReport(this, "a93968489a", false);//正式环境改成false
        Bugly.init(this, "a93968489a",true);
        setContentView(R.layout.vizpower_login_hd);
    }

    public void onClick(View opt) {
        switch (opt.getId()) {
            case R.id.buttonLogin:
                if (new AutonomyUtil().isLockNow(this)){
                    new AutonomyUtil().ToastWhenUnlock(this);
                }
                else{
                    EditText cwd0 = (EditText) findViewById(R.id.editTextUserName);
                    String str1 = cwd0.getText().toString();
                    EditText cwd2 = (EditText) findViewById(R.id.editTextPassword);
                    String str2 = cwd2.getText().toString();
                    if (str1.equals("") || str2.equals("")){
                        Toast.makeText(this, "请输入用户名和密码", Toast.LENGTH_LONG).show();
                    }
                    else {
                        if (str1.equals(str2)){
                            Intent ma = new Intent(this, UIPicker.class);
                            startActivity(ma);
                        }
                        else {
                            Toast.makeText(this, "登录失败", Toast.LENGTH_LONG).show();
                        }
                    }
                }

                break;
            default:
                break;
        }
    }
}