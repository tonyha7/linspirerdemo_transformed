package com.th7.classic;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.csdk.*;
import java.io.File;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import android.app.*;
import androidx.appcompat.app.AppCompatActivity;
import com.lzf.easyfloat.EasyFloat;
import com.lzf.easyfloat.enums.ShowPattern;
import com.lzf.easyfloat.interfaces.OnInvokeView;
import com.th7.utils.ContentUriUtil;
import com.th7.utils.RootCommand;
import com.th7.utils.mdm.AutonomyUtil;
import com.th7.utils.mdm.CSDKUtil;
import com.th7.utils.mdm.MDM;
import com.th7.utils.mdm.MDMType;
import com.th7.wisdomclass.linspirerdemo.BuildConfig;
import com.th7.wisdomclass.linspirerdemo.DeviceMethod;
import com.th7.utils.QRCodeUtil;
import com.th7.wisdomclass.linspirerdemo.R;
import com.th7.utils.PostUtil;
import com.th7.utils.SharedPreferencesUtil;

import es.dmoral.toasty.Toasty;

public class MainActivity extends AppCompatActivity {
    MDM mdm;
    private boolean _float=false;
    private boolean Block_Linspirer_Network=true;
    private String 此版本注意事项="小修";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //禁止截图
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        super.onCreate(savedInstanceState);

        ShowUI();
        /*
        * copied from start2
        *
        *
            依据《中华人民共和国网络安全法》《未成年人网络保护条例》《关于进一步严格管理切实防止未成年人沉迷网络游戏的通知》等相关法律法规，WisdomClass已暂时停止向您提供该服务。
            依据学校《数字班平板管理办法》《手机管理规定》，WisdomClass已阻止该操作。
        **/

        mdm = new MDM(this);
        mdm.load();

        if (is_root()){
            Toasty.warning(this, "检测到root 建议使用Magisk来躲避检查", Toast.LENGTH_SHORT, true).show();
        }
        //PackageUtil pkgutil = new PackageUtil(this);
        DeviceMethod.getInstance(this).protectApp();
    }

    public void onClick(View view) {
        mdm = new MDM(this);
        EditText et2 = (EditText) findViewById(R.id.auth);
        String auth = et2.getText().toString();
            if(!auth.equals(base64_device_gen())){
                Toasty.error(this, "未注册", Toast.LENGTH_SHORT, true).show();
            }
            else{
                SharedPreferencesUtil.setString(this,"newkey",base64_device_gen());
                try {
                    switch (view.getId()){
                        case R.id.AL:
                            startActivityLauncher();
                            break;
                        case R.id.Lock:
                            new AutonomyUtil().LockNow(this);
                            exit();
                            break;
                        case R.id.button18:
                            Block_Linspirer_Network=false;
                            Toasty.warning(this, "本次返回桌面将不会将领创网络写进黑名单", Toast.LENGTH_SHORT, true).show();
                            Toasty.warning(this, "如果你后悔了 请手动断网返回", Toast.LENGTH_SHORT, true).show();
                            break;
                        case R.id.button16:
                            DeviceMethod.getInstance(this).deprotectApp();
                            DeviceMethod.getInstance(this).dedeviceowner();
                            DeviceMethod.getInstance(this).onActivate();
                            Toasty.warning(this, "回到领创后 安装的程序可能被卸载", Toast.LENGTH_SHORT, true).show();
                            break;
                        case R.id.button19:
                            EditText Ed4=(EditText) findViewById(R.id.iceboxapp2);
                            String killname1 = Ed4.getText().toString();
                            if(!TextUtils.isEmpty(killname1)) {
                                killapp(killname1);
                            }
                            break;
                        case R.id.button12:
                            EditText Ed3=(EditText) findViewById(R.id.iceboxapp2);
                            String icename1 = Ed3.getText().toString();
                            if(!TextUtils.isEmpty(icename1)){
                                mdm.controlapp(icename1,true);
                                Toasty.success(this, "解冻 "+icename1, Toast.LENGTH_SHORT, true).show();}
                            break;
                        case R.id.button11:
                            EditText Ed2=(EditText) findViewById(R.id.iceboxapp2);
                            String icename = Ed2.getText().toString();
                            if(!TextUtils.isEmpty(icename)){
                                mdm.controlapp(icename,false);
                                Toasty.success(this, "冻结 "+icename, Toast.LENGTH_SHORT, true).show();}
                            break;

                        case R.id.button13:
                            EditText Ed5=(EditText) findViewById(R.id.iceboxapp2);
                            String icename2 = Ed5.getText().toString();
                            if(!TextUtils.isEmpty(icename2)){
                                DeviceMethod.getInstance(this).deprotectAppbyPackage(icename2);
                                uninstallapp(icename2);
                            }
                            break;

                        case R.id.button10:
                            if(!_float){
                                _float=true;
                                EasyFloat.with(this).setShowPattern(ShowPattern.ALL_TIME).setLayout(R.layout.float_test,new OnInvokeView() {@Override public void invoke(View view) { View click_view_float = view.findViewById(R.id.tvOpenMain);click_view_float.setOnClickListener(new View.OnClickListener() {@Override public void onClick(View v) { exit();}}); }}).show();
                            }
                            else{
                                _float=false;
                                EasyFloat.dismiss();
                            }
                            //startActivity(new Intent().setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings$PowerUsageSummaryActivity")));
                            break;
                        case R.id.button6:
                            killapp("com.zdsoft.newsquirrel");
                            Toasty.success(this, "美师优课死了", Toast.LENGTH_SHORT, true).show();
                            break;
                        case R.id.button7:
                            Intent intent12 = new Intent();
                            intent12.setComponent(new ComponentName("com.android.settings", "com.android.settings.display.NavigationBarSettingsActivity"));
                            startActivity(intent12);
                            break;
                        case R.id.button15:
                            Intent mo = new Intent(MainActivity.this, MoreOptions.class);
                            startActivity(mo);
                            break;
                        case R.id.button5:
                            DeviceMethod.getInstance(this).startLockMethod();
                            break;
                        case R.id.button3:
                            if(!isAppInstalled(this,"com.teslacoilsw.launcher")||!isAppInstalled(this,"com.google.android.apps.nexuslauncher")) {
                                nonovamsg();
                            }
                            else {
                                mdm.load();
                                mdm.FlashAppWhiteList(this);
                                mdm.releasekeys();
                                moveTaskToBack(true);
                            }
                            break;
                        case R.id.button14:
                            Intent FS = new Intent(Intent.ACTION_GET_CONTENT);
                            FS.setType("application/vnd.android.package-archive");
                            startActivityForResult(FS, 1);
                            break;
                        case R.id.button2:
                            AlertDialog.Builder alertdialogbuilder = new AlertDialog.Builder(this);
                            alertdialogbuilder.setMessage("所有这些时刻终将在时光中消逝,\n就像泪水消失在雨中,\nTime to die.\n2021年11月23日");
                            alertdialogbuilder.setPositiveButton("确定", null);
                            alertdialogbuilder.setNeutralButton("关于", new DialogInterface.OnClickListener(){
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(MainActivity.this, aboutactivity.class);
                                    startActivity(intent);
                                }
                            });
                            final AlertDialog alertdialog1 = alertdialogbuilder.create();
                            alertdialog1.show();
                            break;
                        case R.id.button8:
                            Intent it1 = new Intent("android.intent.action.MAIN");
                            it1.setClassName("com.ndwill.swd.appstore","com.ndwill.swd.appstore.activity.MainActivity");
                            startActivity(it1);
                            break;
                        case R.id.button4:
                            EditText et1 = (EditText) findViewById(R.id.appName);
                            String appname = et1.getText().toString();
                            mdm.addAppWhite(appname);
                            Toasty.info(this, "写入 " + appname, Toast.LENGTH_SHORT, true).show();
                            break;
                        case R.id.button:
                            mdm.controlapp("com.ndwill.swd.appstore",true);
                            break;
                        case R.id.button9:
                            mdm.controlapp("com.ndwill.swd.appstore",false);
                            break;

                    }
                }
                catch(Exception e){
                }
            }
    }

    private void startActivityLauncher(){
        Intent SAL = new Intent();
        SAL.setComponent(new ComponentName(BuildConfig.APPLICATION_ID, "de.szalkowski.activitylauncher.MainActivity"));
        startActivity(SAL);
        Toasty.info(this, "原作者@butzist 项目地址:https://github.com/butzist/ActivityLauncher", Toast.LENGTH_SHORT, true).show();
    }
    // 判断是否具有ROOT权限  不会检测面具
    public static boolean is_root(){
        boolean res = false;
        try{
            if ((!new File("/system/bin/su").exists()) &&
                    (!new File("/system/xbin/su").exists())){}
            else {
                res = true;
            };
        }
        catch (Exception e) {
        }
        return res;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mdm = new MDM(this);
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();
            if (null != uri) {
                String path="";
                try{
                    path = ContentUriUtil.getPath(this,uri);
                }
                catch (Exception e){
                    Toasty.error(this, "异常操作 是不是在最近，或者在下载里找到的文件?", Toast.LENGTH_SHORT, true).show();
                }
                if(path==null){
                    String aa=uri.toString();
                    aa=aa.replaceFirst("%3A","/");
                    aa=aa.replaceFirst("content:\\/\\/com\\.android\\.externalstorage\\.documents\\/document\\/", "/storage/");
                    aa=aa.replaceAll("%2F","/");
                    try{
                        PackageManager pm1 = getPackageManager();
                        PackageInfo info1 = pm1.getPackageArchiveInfo(aa, PackageManager.GET_ACTIVITIES);
                        if(info1 != null){
                            ApplicationInfo appInfo1 = info1.applicationInfo;
                            String packageName = appInfo1.packageName;
                            Toasty.info(this, "写入 " + packageName, Toast.LENGTH_SHORT, true).show();
                            mdm.addAppWhite(packageName);
                            Intent intent3 = new Intent(Intent.ACTION_VIEW);
                            intent3.setDataAndType(uri,"application/vnd.android.package-archive");
                            startActivity(intent3);
                        }
                    }
                    catch (Exception e){
                        Toasty.error(this, "异常操作 是不是在最近，或者在下载里找到的文件?", Toast.LENGTH_SHORT, true).show();
                    }
                }
                else{
                    try{
                        PackageManager pm = getPackageManager();
                        PackageInfo info = pm.getPackageArchiveInfo(path, PackageManager.GET_ACTIVITIES);
                        if(info != null){
                            ApplicationInfo appInfo = info.applicationInfo;
                            String packageName = appInfo.packageName;
                            Toasty.info(this, "写入 " + packageName, Toast.LENGTH_SHORT, true).show();
                            //new MiaMdmPolicyManager(this).appWhiteListWrite(list3);
                            mdm.addAppWhite(packageName);
                            Intent intent1 = new Intent(Intent.ACTION_VIEW);
                            intent1.setDataAndType(uri,"application/vnd.android.package-archive");
                            startActivity(intent1);
                        }
                    }
                    catch (Exception e){
                        Toasty.error(this, "异常操作 是不是在最近，或者在下载里找到的文件?", Toast.LENGTH_SHORT, true).show();
                    }
                }
            }
        }
    }


    public static String getWifiMacAddress() {
        String defaultMac = "02:00:00:00:00:00";
        try {
            List<NetworkInterface> interfaces = Collections
                    .list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface ntwInterface : interfaces) {
                if (ntwInterface.getName().equalsIgnoreCase("wlan0")) {//之前是p2p0，修正为wlan
                    byte[] byteMac = ntwInterface.getHardwareAddress();
                    if (byteMac == null) {
                        // return null;
                    }
                    StringBuilder strBuilder = new StringBuilder();
                    for (int i = 0; i < byteMac.length; i++) {
                        strBuilder.append(String
                                .format("%02X:", byteMac[i]));
                    }
                    if (strBuilder.length() > 0) {
                        strBuilder.deleteCharAt(strBuilder.length() - 1);
                    }
                    return strBuilder.toString();
                }
            }
        } catch (Exception e) {
        }
        return defaultMac;
    }

    //注册//
    private String base64_device_gen(){
        String eS = base64_device_info();
        String ans1,ans2;
        ans1=eS.substring(eS.length()-4);
        ans2=eS.substring(0,2);
        return ans1+ans2;
    }

    //设备id//
    private String base64_device_info(){
        String ba;
        ba=getWifiMacAddress();
        return Base64.encodeToString(ba.getBytes(), Base64.NO_WRAP);
    }


    public void ShowUI() {
        getSupportActionBar().show();
        setContentView(R.layout.activity_main);
        TextView tv3 = (TextView) findViewById(R.id.textView9);
        ImageView mImageView = (ImageView) findViewById(R.id.iv);
        Bitmap mBitmap = QRCodeUtil.createQRCodeBitmap(base64_device_info(), 480, 480);
        mImageView.setImageBitmap(mBitmap);

        if(SharedPreferencesUtil.hasKey(this,"newkey")){
            String newkey = SharedPreferencesUtil.getString(this,"newkey","");
            TextView tvkey = (TextView) findViewById(R.id.auth);
            tvkey.setText(newkey);
        }

        tv3.setText("version:" + this.getPackageName() + "_" + BuildConfig.VERSION_NAME);
        if (!SharedPreferencesUtil.hasKey(this,"isFirstRun_" + BuildConfig.VERSION_NAME)){
          SharedPreferencesUtil.setBoolean(this,"isFirstRun_" + BuildConfig.VERSION_NAME,true);
        }
        if (SharedPreferencesUtil.getBoolean(this, "isFirstRun_" + BuildConfig.VERSION_NAME, true)) {
            msg();
        }

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode==25){
            DeviceMethod.getInstance(this).LockNow();
            /*
            //new MiaMdmPolicyManager(this).setHomeKey(true);
            new CSDKManager(this).hideHomeSoftKey(false);
            new CSDKManager(this).hideMenuSoftKey(false);
            try {
                new CSDKManager(this).setPackageEnabled("com.android.launcher3",true);
            } catch (Exception e) {
            }
            //try {
            //new MiaMdmPolicyManager(this).controlApp("com.android.launcher3",false);
            //} catch (Exception e) {
            //}

            //恢复出厂设置
            //DeviceMethod.getInstance(this).WipeData();

            //没设备管理器就卸载
            ArrayList euninstall=new ArrayList<>();
            euninstall.add(BuildConfig.APPLICATION_ID);
            new CSDKManager(this).addUninstallPackageWhiteList(euninstall);
            new CSDKManager(this).silentUnInstall(BuildConfig.APPLICATION_ID);
            //new MiaMdmPolicyManager(this).silentUnInstall(getPacakge());

             */
            finish();
        }

        if(keyCode==3){
            System.exit(0);
            return false;
        }
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            exit();
        }
        return true;
    }

    public void exit(){
        EasyFloat.dismiss();
        mdm = new MDM(this);
        mdm.back2linspirer();
        Intent it1 = new Intent("android.intent.action.MAIN");
        it1.setClassName("com.android.launcher3", "com.android.launcher3.LauncherNew");
        startActivity(it1);
        if (Block_Linspirer_Network){
            mdm.Block_Linspirer_Network();
        }
        mdm.releasekeys();
        moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    /**
     * 判断手机是否安装某个应用
     *
     * @param packageName 应用包名
     * @return true：安装，false：未安装
     */
    public boolean isAppInstalled(Context context, String packageName) {
        final PackageManager packageManager = context.getPackageManager();
        List<PackageInfo> pinfo = packageManager.getInstalledPackages(0);
        List<String> pName = new ArrayList<String>();
        if (pinfo != null) {
            for (int i = 0; i < pinfo.size(); i++) {
                String pn = pinfo.get(i).packageName;
                pName.add(pn);
            }
        }
        return pName.contains(packageName);
    }

    private void nonovamsg(){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("温馨提示");
        builder.setMessage("貌似没有Nova Launcher呢\n是否先用活动启动器代替一下？");
        builder.setIcon(R.drawable.nene);
        builder.setCancelable(true);            //点击对话框以外的区域是否让对话框消失

        //设置正面按钮
        builder.setPositiveButton("活动启动器", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivityLauncher();
                dialog.dismiss();
            }
        });
        //设置反面按钮
        builder.setNegativeButton("我有其他Launcher", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mdm.releasekeys();
                moveTaskToBack(true);
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();      //创建AlertDialog对象
        dialog.show();
    }

    private void msg(){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("温馨提示");
        builder.setMessage(此版本注意事项);
        builder.setIcon(R.drawable.nene);
        builder.setCancelable(false);            //点击对话框以外的区域是否让对话框消失

        //设置正面按钮
        builder.setPositiveButton("我同意", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                PostUtil.sendPost(getApplicationContext(),"设备首次运行此版本并同意注意事项");
                SharedPreferencesUtil.setBoolean(getApplicationContext(),"isFirstRun_"+BuildConfig.VERSION_NAME,false);
                dialog.dismiss();
            }
        });
        //设置反面按钮
        builder.setNegativeButton("我不同意", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                PostUtil.sendPost(getApplicationContext(),"用户不同意注意事项");
                SharedPreferencesUtil.setBoolean(getApplicationContext(),"isFirstRun_"+BuildConfig.VERSION_NAME,true);
                exit();
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();      //创建AlertDialog对象
        dialog.show();
    }

    private void killapp(String PackageName){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("强杀进程 "+PackageName);
        builder.setMessage("请选择方式:");
        builder.setIcon(R.drawable.nene);
        builder.setCancelable(true);            //点击对话框以外的区域是否让对话框消失

        //设置正面按钮
        builder.setPositiveButton("管控接口", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mdm = new MDM(MainActivity.this);
                mdm.KillAppBackGround(PackageName);
                dialog.dismiss();
            }
        });
        //设置反面按钮
        builder.setNegativeButton("Shell(需ROOT)", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                RootCommand.runCommand("am force-stop "+PackageName);
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();      //创建AlertDialog对象
        dialog.show();
    }

    private void uninstallapp(String PackageName){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("卸载 "+PackageName);
        builder.setMessage("请选择方式:");
        builder.setIcon(R.drawable.nene);
        builder.setCancelable(true);            //点击对话框以外的区域是否让对话框消失

        //设置正面按钮
        builder.setPositiveButton("正常卸载", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mdm = new MDM(MainActivity.this);
                mdm.uninstallApp(PackageName);
                dialog.dismiss();
            }
        });
        //设置反面按钮
        builder.setNegativeButton("保留数据(需ROOT)", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                RootCommand.runCommand("pm uninstall -k "+PackageName);
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();      //创建AlertDialog对象
        dialog.show();
    }
}
