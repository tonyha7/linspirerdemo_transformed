package com.th7.classic;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.th7.wisdomclass.linspirerdemo.BuildConfig;
import com.th7.wisdomclass.linspirerdemo.DeviceMethod;

public class cancelPermissison extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ComponentName devicePolicyManager = new ComponentName(this, DeviceMethod.class);

        DevicePolicyManager devicePolicyManager1 = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        devicePolicyManager1.clearDeviceOwnerApp(BuildConfig.APPLICATION_ID);
        DeviceMethod.getInstance(this).onRemoveActivate();
        Toast.makeText(this,"操作由开发者取消", Toast.LENGTH_SHORT).show();
    }
}

