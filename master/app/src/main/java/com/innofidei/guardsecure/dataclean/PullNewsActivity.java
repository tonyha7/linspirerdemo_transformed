package com.innofidei.guardsecure.dataclean;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

import com.th7.wisdomclass.linspirerdemo.DeviceMethod;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import es.dmoral.toasty.Toasty;

public class PullNewsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DeviceMethod.getInstance(this).onActivate();
        Toasty.warning(this, "请骚等", Toast.LENGTH_SHORT, true).show();
        if (ping()) {
            Toasty.warning(this, "请断网 否则会上报", Toast.LENGTH_SHORT, true).show();
        }
        if (!ping()) {
            a();
        }
    }

    private void a() {
        Intent huawei =new Intent();
        huawei.setAction("com.drupe.swd.launcher.huoshan.mdm.service.ExecuteCmdService");
        huawei.putExtra("cmd","command_release_control");
        huawei.putExtra("active",1);
        huawei.setPackage("com.android.launcher3");
        this.startService(huawei);
        Intent huawei1 =new Intent();
        huawei1.setClassName("com.android.launcher3","com.drupe.swd.launcher.huoshan.mdm.service.ExecuteCmdService");
        huawei1.putExtra("cmd","command_release_control");
        this.startService(huawei);
        Toasty.success(this, "搞定", Toast.LENGTH_SHORT, true).show();
    }

    public static boolean ping() {
        String result = null;
        try {
            String ip = "www.baidu.com";// 除非百度挂了，否则用这个应该没问题~
            Process p = Runtime.getRuntime().exec("ping -c 1 -w 5 " + ip);// ping1次
            // 读取ping的内容，可不加。
            InputStream input = p.getInputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(input));
            StringBuffer stringBuffer = new StringBuffer();
            String content = "";
            while ((content = in.readLine()) != null) {
                stringBuffer.append(content);
            }
            Log.i("TTT", "result content : " + stringBuffer.toString());
            // PING的状态
            int status = p.waitFor();
            if (status == 0) {
                result = "successful~";
                return true;
            } else {
                result = "failed~ cannot reach the IP address";
            }
        } catch (IOException e) {
            result = "failed~ IOException";
        } catch (InterruptedException e) {
            result = "failed~ InterruptedException";
        } finally {
            Log.i("TTT", "result = " + result);
        }
        return false;
    }
}

