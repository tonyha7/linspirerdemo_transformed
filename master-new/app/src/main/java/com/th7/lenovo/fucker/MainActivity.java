package com.th7.lenovo.fucker;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.csdk.*;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lzf.easyfloat.EasyFloat;
import com.lzf.easyfloat.enums.ShowPattern;
import com.lzf.easyfloat.interfaces.OnInvokeView;

import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import es.dmoral.toasty.Toasty;


public class MainActivity extends AppCompatActivity {
    private CSDKManager csdk;

    SharedPreferences spfs;
    SharedPreferences spfs1;
    private boolean _float=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DeviceMethod.getInstance(this).onActivate();
        csdk=new CSDKManager(this);
        ShowUI();
        Load_csdk();
    }
    public void onClick(View view) {
        EditText et2 = (EditText) findViewById(R.id.auth);
        String auth = et2.getText().toString();
        if(auth.equals(device_gen())){
            spfs=getSharedPreferences("data",MODE_PRIVATE);
            SharedPreferences.Editor editor=spfs.edit();
            editor.putString("key",device_gen());
            editor.apply();
            try {
                switch (view.getId()) {
                    case R.id.button11:
                        if(!_float){
                            _float=true;
                            EasyFloat.with(this).setShowPattern(ShowPattern.ALL_TIME).setLayout(R.layout.float_test,new OnInvokeView() {@Override public void invoke(View view) { View click_view_float = view.findViewById(R.id.tvOpenMain);click_view_float.setOnClickListener(new View.OnClickListener() {@Override public void onClick(View v) { exit_csdk();}}); }}).show();
                        }
                        else{
                            _float=false;
                            EasyFloat.dismiss();
                        }
                        //startActivity(new Intent().setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings$PowerUsageSummaryActivity")));
                        break;
                    case R.id.reload:
                        Load_csdk();
                        break;
                    case R.id.button10:
                        new CSDKManager(this).killApplicationProcess("com.zdsoft.newsquirrel");
                        break;
                    case R.id.button3:
                        new CSDKManager(this).setCustomOTG(true);
                        new CSDKManager(this).allowTFcard(true);
                        new CSDKManager(this).enableMassStorage(true);
                        new CSDKManager(this).disableAutostart(true);
                        Intent FS = new Intent(Intent.ACTION_GET_CONTENT);
                        FS.setType("application/vnd.android.package-archive");
                        startActivityForResult(FS, 1);
                        break;
                    case R.id.button9:
                        new CSDKManager(this).disableCamera(true);
                        toast("关闭相机",false);
                        break;
                    case R.id.button8:
                        new CSDKManager(this).disableCamera(false);
                        toast("打开相机",true);
                        break;
                    case R.id.button7:
                        DeviceMethod.getInstance(this).deprotectApp();
                        DeviceMethod.getInstance(this).allowFactoryReset();
                        DeviceMethod.getInstance(this).dedeviceowner();
                        toast("取消保护应用",true);
                        break;
                    case R.id.button5:
                        EditText Ed3=(EditText) findViewById(R.id.apppackage2);
                        String jdapppackage = Ed3.getText().toString();
                        if(!TextUtils.isEmpty(jdapppackage)){
                            try {new CSDKManager(this).setPackageEnabled(jdapppackage, true);} catch (Exception e) {}
                            toast("解冻 "+jdapppackage,true);
                        }
                        break;
                    case R.id.button4:
                        EditText Ed2=(EditText) findViewById(R.id.apppackage2);
                        String djapppackage = Ed2.getText().toString();
                        if(!TextUtils.isEmpty(djapppackage)){
                            try {new CSDKManager(this).setPackageEnabled(djapppackage, false);} catch (Exception e) {}
                            toast("冻结 "+djapppackage,true);
                        }
                        break;
                    case R.id.button6:
                        home();
                        moveTaskToBack(true);
                        new CSDKManager(this).killApplicationProcess(BuildConfig.APPLICATION_ID);
                        finish();
                        break;
                    case R.id.button:
                        EditText Ed4=(EditText) findViewById(R.id.apppackage2);
                        String uninstallpackage = Ed4.getText().toString();
                        if(!TextUtils.isEmpty(uninstallpackage)) {
                            DeviceMethod.getInstance(this).deprotectAppbyPackage(uninstallpackage);
                            new CSDKManager(this).silentUnInstall(uninstallpackage);
                            toast("卸载 " + uninstallpackage, true);
                        }
                            break;
                    case R.id.button2:
                        EditText et1 = (EditText) findViewById(R.id.apppackage);
                        String whiteappname = et1.getText().toString();
                        ArrayList list2;
                        if(!whiteappname.equals("null")){
                            if(!TextUtils.isEmpty(whiteappname)) {
                                list2 = (ArrayList) new CSDKManager(this).getInstallPackageWhiteList();
                                list2.add(whiteappname);
                                new CSDKManager(this).addInstallPackageWhiteList(list2);
                                toast("写入 " + whiteappname, true);
                            }
                        }
                        else {
                            toast("写入null将导致领创上报", false);
                        }

                        break;
                    default:
                        break;
                }
                } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else{
            toast("未注册",false);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();
            if (null != uri) {
                String path="";
                try{
                    path =ContentUriUtil.getPath(this,uri);
                }
                catch (Exception e){
                    Toasty.error(this, "异常操作 是不是在最近，或者在下载里找到的文件?", Toast.LENGTH_SHORT, true).show();
                }
                if(path==null){
                    String aa=uri.toString();
                    aa=aa.replaceFirst("%3A","/");
                    aa=aa.replaceFirst("content:\\/\\/com\\.android\\.externalstorage\\.documents\\/document\\/", "/storage/");
                    aa=aa.replaceAll("%2F","/");
                    try{
                        PackageManager pm1 = getPackageManager();
                        PackageInfo info1 = pm1.getPackageArchiveInfo(aa, PackageManager.GET_ACTIVITIES);
                        if(info1 != null){
                            ApplicationInfo appInfo1 = info1.applicationInfo;
                            String packageName = appInfo1.packageName;
                            Toasty.info(this, "写入 " + packageName, Toast.LENGTH_SHORT, true).show();
                            ArrayList list4;
                            list4 = (ArrayList) new CSDKManager(this).getInstallPackageWhiteList();
                            list4.add(packageName);
                            new CSDKManager(this).addInstallPackageWhiteList(list4);
                            Intent intent3 = new Intent(Intent.ACTION_VIEW);
                            intent3.setDataAndType(uri,"application/vnd.android.package-archive");
                            startActivity(intent3);
                        }
                    }
                    catch (Exception e){
                        Toasty.error(this, "异常操作 是不是在最近，或者在下载里找到的文件?", Toast.LENGTH_SHORT, true).show();
                    }
                }
                else{
                    try{
                        PackageManager pm = getPackageManager();
                        PackageInfo info = pm.getPackageArchiveInfo(path, PackageManager.GET_ACTIVITIES);
                        if(info != null){
                            ApplicationInfo appInfo = info.applicationInfo;
                            String packageName = appInfo.packageName;
                            Toasty.info(this, "写入 " + packageName, Toast.LENGTH_SHORT, true).show();
                            //new MiaMdmPolicyManager(this).appWhiteListWrite(list3);
                            ArrayList list3;
                            list3 = (ArrayList) new CSDKManager(this).getInstallPackageWhiteList();
                            list3.add(packageName);
                            new CSDKManager(this).addInstallPackageWhiteList(list3);
                            Intent intent1 = new Intent(Intent.ACTION_VIEW);
                            intent1.setDataAndType(uri,"application/vnd.android.package-archive");
                            startActivity(intent1);
                        }
                    }
                    catch (Exception e){
                        Toasty.error(this, "异常操作 是不是在最近，或者在下载里找到的文件?", Toast.LENGTH_SHORT, true).show();
                    }
                }
            }
        }
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode==25){
            DeviceMethod.getInstance(this).LockNow();
            finish();
        }

        if(keyCode==3){
            System.exit(0);
            return false;
        }
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            exit_csdk();
        }
        return true;
    }
    private void exit_csdk(){
        if (_float){
            EasyFloat.dismiss();
        }

        Intent b2l = new Intent(MainActivity.this, Back2Linspirer.class);
        startActivity(b2l);
        finish();
    }
    private void ShowUI(){
        setContentView(R.layout.activity_main);
        ImageView mImageView = (ImageView) findViewById(R.id.iv);
        Bitmap mBitmap = QRCodeUtil.createQRCodeBitmap(device_info(), 480, 480);
        mImageView.setImageBitmap(mBitmap);
        spfs1 = getSharedPreferences("data", MODE_PRIVATE);
        String KEY = spfs1.getString("key", "");
        TextView tvkey = (TextView) findViewById(R.id.auth);
        tvkey.setText(KEY);
    }

    private void Load_csdk(){

        //csdk
        try {
            new CSDKManager(this).setPackageEnabled("com.android.launcher3", false);
        } catch (Exception e) {
            toast(e.toString(),false);
        }

        try{
            new CSDKManager(this).setDeviceOwner(BuildConfig.APPLICATION_ID+"/com.th7.lenovo.fucker.DeviceReceiver");
        }
        catch (Exception E){
            toast(E.toString(),false);
        }

        DeviceMethod.getInstance(this).disallowFactoryReset();
        DeviceMethod.getInstance(this).protectApp();

        new CSDKManager(this).disableAutostart(true);
        new CSDKManager(this).hideBackSoftKey(false);
        new CSDKManager(this).hideHomeSoftKey(true);
        new CSDKManager(this).hideMenuSoftKey(true);
        new CSDKManager(this).setSafeModeDisabled(false);
        new CSDKManager(this).disableHiddenGame(false);
        new CSDKManager(this).disableWifiDirect(false);
        new CSDKManager(this).enableDevMode(true);
        new CSDKManager(this).disableWifi(false);
        new CSDKManager(this).SetEnable(false);
        new CSDKManager(this).enableMassStorage(true);
        new CSDKManager(this).setUsbCharging(false);
        new CSDKManager(this).enableUsbDebugging(true);
        new CSDKManager(this).setCustomOTG(true);
        new CSDKManager(this).allowTFcard(true);
        //新增
        new CSDKManager(this).disableInstallation(false);
        new CSDKManager(this).disableStatusBarNotification(false);
        new CSDKManager(this).disableStatusBarPanel(false);
        new CSDKManager(this).disableUnInstallation(false);
        new CSDKManager(this).enableSIM(true);
        new CSDKManager(this).enableData(true);
        new CSDKManager(this).fullScreenForever(false);
        new CSDKManager(this).disableBluetooth(false);
        new CSDKManager(this).allowBluetooth(true);
        new CSDKManager(this).allowBluetoothDataTransfer(true);
        new CSDKManager(this).disableBluetoothShare(false);

        toast("CSDK加载完毕",true);
        }

    /**
     * @return wifi下mac
     */
    public String WifiMacAddress() {
        try {
            List<NetworkInterface> interfaces = Collections
                    .list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface ntwInterface : interfaces) {
                if (ntwInterface.getName().equalsIgnoreCase("wlan0")) {//之前是p2p0，修正为wlan
                    byte[] byteMac = ntwInterface.getHardwareAddress();
                    if (byteMac == null) {
                        // return null;
                    }
                    StringBuilder strBuilder = new StringBuilder();
                    for (int i = 0; i < byteMac.length; i++) {
                        strBuilder.append(String
                                .format("%02X:", byteMac[i]));
                    }
                    if (strBuilder.length() > 0) {
                        strBuilder.deleteCharAt(strBuilder.length() - 1);
                    }
                    return strBuilder.toString();
                }
            }
        } catch (Exception e) {
        }
        return "02:00:00:00:00:00";
    }
    private String sn(){
        return new CSDKManager(this).getDeviceInfo(2);
    }
    //注册//
    private String device_gen(){
        String eS = device_info();
        String ans1,ans2,sb;
        ans1=eS.substring(eS.length()-2);
        ans2=eS.substring(0,2);
        sb=ans1+ans2;
        return Base64.encodeToString(sb.getBytes(), Base64.NO_WRAP);
    }

    //设备id//
    private String device_info(){
        String ba=sn()+WifiMacAddress();
        return Base64.encodeToString(ba.getBytes(), Base64.NO_WRAP);
    }


    public void home(){
        new CSDKManager(this).hideHomeSoftKey(false);
        new CSDKManager(this).hideBackSoftKey(false);
        new CSDKManager(this).hideMenuSoftKey(false);
    }

    public void toast(String msg,boolean isRightToast){
        if (isRightToast){
            Toasty.success(this, msg, Toast.LENGTH_SHORT, true).show();
        }
        else{
            Toasty.error(this, msg, Toast.LENGTH_SHORT, true).show();
        }
    }

}