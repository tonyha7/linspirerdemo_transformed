package com.th7.lenovo.fucker;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.UserManager;
import android.widget.Toast;

import java.util.List;

import es.dmoral.toasty.Toasty;

public class DeviceMethod {
    private static DeviceMethod mDeviceMethod;

    private DevicePolicyManager devicePolicyManager;
    private ComponentName componentName;
    private Context mContext;

    private DeviceMethod (Context context){
        mContext=context;
        //获取设备管理服务
        devicePolicyManager=(DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        //DeviceReceiver 继承自 DeviceAdminReceiver
        componentName=new ComponentName(context, DeviceReceiver.class);
    }

    public static DeviceMethod getInstance(Context context){
        if (mDeviceMethod==null) {
            synchronized (DeviceMethod.class) {
                if (mDeviceMethod==null) {
                    mDeviceMethod=new DeviceMethod(context);
                }
            }
        }
        return mDeviceMethod;
    }
    // 激活程序
    // 激活程序
    public void onActivate() {
        //判断是否激活  如果没有就启动激活设备
        if (!devicePolicyManager.isAdminActive(componentName)) {
            Intent intent = new Intent(
                    DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN,
                    componentName);
            intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "防止程序被领创卸载");
            mContext.startActivity(intent);
        }
    }

    /**
     * 移除程序 如果不移除程序 APP无法被卸载
     */
    public void onRemoveActivate() {
        //Toasty.success(mContext.getApplicationContext(), "已阻止取消设备管理器 如果有需要请去更多选项", Toast.LENGTH_SHORT, true).show();
        devicePolicyManager.removeActiveAdmin(componentName);
    }



    public void disallowFactoryReset() {
        if (devicePolicyManager.isDeviceOwnerApp(BuildConfig.APPLICATION_ID)) {
            devicePolicyManager.addUserRestriction(componentName, UserManager.DISALLOW_FACTORY_RESET);
        }
    }

    public void allowFactoryReset() {
        if(devicePolicyManager.isDeviceOwnerApp(BuildConfig.APPLICATION_ID)){
            devicePolicyManager.clearUserRestriction(componentName,UserManager.DISALLOW_FACTORY_RESET);
            //devicePolicyManager.clearDeviceOwnerApp(BuildConfig.APPLICATION_ID);
        }
    }
    public void dedeviceowner() {
        devicePolicyManager.clearDeviceOwnerApp(BuildConfig.APPLICATION_ID);
    }

    public void protectApp() {
        PackageManager pm = mContext.getPackageManager();
        List<PackageInfo> packages = pm.getInstalledPackages(0);
        if (devicePolicyManager.isDeviceOwnerApp(BuildConfig.APPLICATION_ID)) {
            //devicePolicyManager.addUserRestriction(componentName, UserManager.DISALLOW_FACTORY_RESET);
            for (PackageInfo packageInfo : packages) {
                if ((packageInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {
                    if ("com.android.launcher3".equals(packageInfo.packageName) || "com.ndwill.swd.appstore".equals(packageInfo.packageName)) {
                        continue;
                    }
                    if (!devicePolicyManager.isUninstallBlocked(componentName, packageInfo.packageName)) {
                        Toasty.success(mContext.getApplicationContext(), "已经添加" + packageInfo.packageName + "至保护（防卸载）名单", Toast.LENGTH_SHORT, true).show();
                        devicePolicyManager.setUninstallBlocked(componentName, packageInfo.packageName, true);
                    }
                }
            }
        }
    }

    public void deprotectAppbyPackage(String packageName) {
        if (devicePolicyManager.isDeviceOwnerApp(BuildConfig.APPLICATION_ID)) {
            devicePolicyManager.setUninstallBlocked(componentName, packageName, false);
        }
    }

    public void deprotectApp() {
        PackageManager pm = mContext.getPackageManager();
        List<PackageInfo> packages = pm.getInstalledPackages(0);
        if(devicePolicyManager.isDeviceOwnerApp(BuildConfig.APPLICATION_ID)){
            for (PackageInfo packageInfo : packages) {
                if ((packageInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0){
                    if(devicePolicyManager.isUninstallBlocked(componentName,packageInfo.packageName)){
                        devicePolicyManager.setUninstallBlocked(componentName,packageInfo.packageName,false);
                    }
                }
            }
        }
    }

    /**
     * 立刻锁屏
     */
    public void LockNow() {
        if (devicePolicyManager.isAdminActive(componentName)) {
            devicePolicyManager.lockNow();
        }else {
            onActivate();
        }
    }

    /**
     * 恢复出厂设置
     */
    public void WipeData() {
        if (devicePolicyManager.isAdminActive(componentName)) {
            devicePolicyManager.wipeData(DevicePolicyManager.WIPE_EXTERNAL_STORAGE);
        }else {
            onActivate();
        }
    }

}



