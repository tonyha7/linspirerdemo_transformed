package com.th7.lenovo.fucker;

import android.app.csdk.CSDKManager;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.lzf.easyfloat.EasyFloat;

public class Back2Linspirer extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        exit_csdk();
    }

    public void exit_csdk(){
        //new MiaMdmPolicyManager(this).setHomeKey(false);
        new CSDKManager(this).hideHomeSoftKey(false);
        new CSDKManager(this).hideBackSoftKey(false);
        new CSDKManager(this).hideMenuSoftKey(false);

        try {
            new CSDKManager(this).setPackageEnabled("com.android.launcher3", true);
        } catch (Exception e) {
        }

        Intent it1 = new Intent("android.intent.action.MAIN");
        it1.setClassName("com.android.launcher3", "com.android.launcher3.LauncherNew");
        startActivity(it1);
        new CSDKManager(this).disableWifi(false);
        new CSDKManager(this).SetEnable(true);
        moveTaskToBack(true);

        new CSDKManager(this).killApplicationProcess(BuildConfig.APPLICATION_ID);
        finish();
    }
}
